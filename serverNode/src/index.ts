import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import indexRoutes from './routes/indexRoutes';
import gamesRoutes from './routes/gamesRoutes';

class Server{

	private app:Application;

	constructor(){
		this.app = express();
		this.config();
		this.middlewares();
		this.routes();
	}

	public config():void{
		this.app.set('port', process.env.PORT || 3000);
	}

	public middlewares():void{
		this.app.use(morgan('dev'));
		this.app.use(express.json());
		this.app.use(express.urlencoded({extended: false}));
		this.app.use(helmet());
		this.app.use(compression());
		this.app.use(cors());
	}

	public routes():void{
		this.app.use('/',indexRoutes);
		this.app.use('/api/games',gamesRoutes);
	}

	public async start(){
		await this.app.listen(this.app.get('port'),() => {
			console.log('Server on port', this.app.get('port'));
		});
	}	
}

const server = new Server();
server.start();