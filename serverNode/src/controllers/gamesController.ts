import { Request, Response } from 'express';
import db from'../database/database';

class GamesController{
	public async listGames(req:Request, res:Response):Promise<Response>{
		const games = await db.query('SELECT * FROM games');
		return res.json(games[0]);		
	}

	public async getOneGame(req:Request, res:Response):Promise<Response>{
		const id = req.params.id;
		const game = await db.query('SELECT * FROM games WHERE id = ?',[id]);
		return res.json(game[0]);
	}	

	public async createGame(req:Request, res:Response):Promise<void>{
		await db.query('INSERT INTO games SET ?',[req.body]);
		res.json({ message:'Game Saved' });
	}

	public async updateGame(req:Request, res:Response):Promise<Response>{
		const id = req.params.id;
		await db.query('UPDATE games SET ? WHERE id = ?',[req.body,id]);
		return res.json({ message:'Game Update' });		
	}	

	public async deleteGame(req:Request, res:Response):Promise<void>{
		const id = req.params.id;
		await db.query('DELETE FROM games WHERE id = ?',[id]);
		res.json({ message:'Game Deleted' });		
	}	
}

const gamesController = new GamesController();
export default gamesController;