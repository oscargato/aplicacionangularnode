import { Request, Response, Router } from 'express';
import gamesController from '../controllers/gamesController';

class GamesRoutes{

	public router: Router;

	constructor(){
		this.router = Router();
		this.config();
	}

	public config():void{
		this.router.get('/',gamesController.listGames);
		this.router.get('/:id',gamesController.getOneGame);
		this.router.post('/',gamesController.createGame);
		this.router.put('/:id',gamesController.updateGame);
		this.router.delete('/:id',gamesController.deleteGame);
	}
}

const gamesRoutes = new GamesRoutes();
export default gamesRoutes.router;